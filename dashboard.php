<?php
	session_start();
	require 'inc/db.php';

	if(!isset($_SESSION['log'])){
		header('Location: login.php');
	}

	$logs = $conn->query('SELECT * FROM log');
	$count = 1;
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Dashboard</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</head>
	<body>
		<div class="container-fluid my-3">
			<p>Selamat datang, <b><?= $_SESSION['username'] ?></b></p>
			<?= (isset($_SESSION['errlog'])) ? "<div class='alert alert-danger'>Username atau password anda salah.</div>" : "" ?>
			<div class="row">
				<?php if(isset($_SESSION['success'])): ?>
					<div class="col-md-12">
						<?php
							if($_SESSION['success']){
								echo "<div class='alert alert-success'>Gambar berhasil diunggah.</div>";
							}else{
								echo "<div class='alert alert-danger'>Gambar gagal diunggah.</div>";
							}
						?>
					</div>
				<?php endif; ?>
				<div class="col-md-6">
					<div class="card">
						<div class="card-body">
							<form method="POST" action="upload.php" enctype="multipart/form-data">
								<div class="form-group">
									<label>Gambar</label>
									<input type="file" name="gambar" class="form-control">
								</div>
								<input type="submit" value="Upload" class="btn btn-primary">
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="card">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th>Nama File</th>
									<th>Penggungah</th>
									<th>Waktu Unggah</th>
								</tr>
							</thead>
							<tbody>
								<?php while ($log = $logs->fetch_array()): ?>
								<tr>
									<?php $file = explode('/', $log['nama_file']); ?>
									<td><?= $count ?></td>
									<td><?= array_pop($file) ?></td>
									<td><?= $log['uploader'] ?></td>
									<td><?= $log['waktu_upload'] ?></td>
									<?php $count++; ?>
								<?php endwhile; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
<?php unset($_SESSION['success']); ?>